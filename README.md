# Owl's Package Manager
```
   ___
  <^,^>     Owl's
  (*-*)     Package
 ┌─"─"─┐    Manager
 | PKG |
 └─────┘
```
## Overview:
opm unifies APT (Debian), DNF (Fedora), zypper (SuSE), pacman/yay (Arch), xbps (Void) and apk (Alpine)
into one (1) syntax.

## Usage:
`opm [operation] {option<s>} {package<s>}`

Supported operations:
```
    " install <package(s)>    Installs package(s)"
    " remove <package(s)>     Removes package(s)"
    " search <package>        Searches repos for package"
    " info <package(s)>       Print info about package(s)"
    " update                  Update repositories"
    " upgrade                 Upgrades packages"
    " autoremove              Autoremoves orphaned packages"
    " dist-upgrade            Upgrades distro to newer version"
    " help                    Print help"
    " version                 Print version"

Supported options:
    " -y | --noconfirm        Assumes 'yes'"
    " --aur                   Uses 'yay' instead of 'pacman' on Arch"
    " --reboot                Reboots system after command is done"
    " --shutdown              Shuts down system after command is done"
```
## Install:
Currently there are two (2) ways to install opm.

### Use the installer
- `git clone https://gitlab.com/shellowl/opm.git`
- `cd opm`
- `./install.sh`

### Copy the script
- `git clone https://gitlab.com/shellowl/opm.git`
- `sudo cp opm/opm /usr/local/bin/`
