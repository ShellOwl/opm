#!/bin/bash
# opm installer


SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"

f_install() {
        chmod +x ${SCRIPTPATH}/opm
        if [ -z $U_PATH ];then
            sudo cp ${SCRIPTPATH}/opm /usr/local/bin/
            echo "opm installed to /usr/local/bin"
        else
            sudo cp ${SCRIPTPATH}/opm ${U_PATH}
            echo "opm installed to ${U_PATH}"
        fi

        echo "Type 'opm --help' to see how it works"
        echo "Report any bugs to https://gitlab.com/ShellOwl/opm"
        exit 0 
}

echo "Welcome to the opm installer"
echo "This script installs opm into /usr/local/bin/"
echo "To install press [Y], to change the path press [P], to cancel the installation press [N]"

read U_INPUT

case $U_INPUT in

    Y | y )
        f_install
        ;;

    P | p)
        echo "Please enter a new path: "
        read U_PATH
        echo "Install opm into $U_PATH"
        f_install
        ;;

    N | n | *)
        echo "Installation canceled"
        exit 0
        ;;
esac
